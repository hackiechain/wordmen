from bulbs.model import Node, Relationship
from bulbs.property import String, Integer, DateTime
from bulbs.utils import current_timestamp

class Word(Node):
    element_type = "word"
    name = String(nullable=False, indexed=True)
    weight = Integer(default=0, nullable=False)
    definitions_zh = String(default="")
    definitions_en = String(default="")
    example_sentence = String(default="")
    popularity = Integer(default= -1)
    
class Similar(Relationship):
    label = "Similar"

class Synonym(Relationship):
    label = "Synonym"

class SameStem(Relationship):
    label = "SameStem"


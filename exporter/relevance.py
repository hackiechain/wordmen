from exporter import Exporter
from random import shuffle

class Relevance(Exporter):
    def init(self, start_word, max_count=90, search_type="DFS"):
        self.start_word = start_word
        self.max_count = int(max_count) 
        if search_type == "DFS":
            self.export = self.dfs_export
        elif search_type == "BFS":
            self.export = self.bfs_export
        else:
            raise Exception("Unknown search_type")
        
    def dfs_export(self):
        word_obj = self.g.word.index.lookup(name=self.start_word).next()
        word_tree = {}
        tree_key = [word_obj.eid]
        word_tree[word_obj.eid] = []
        index = 0 
        while index != len(word_tree) and len(tree_key) < self.max_count:
            near_words = self.g.word.get(tree_key[index]).bothV()
            if near_words == None:
                index += 1
                continue
            for i in near_words:
                if i.eid not in tree_key and i.eid not in word_tree[tree_key[index]]:
                    tree_key.append(i.eid)
                    word_tree[i.eid] = []
                    word_tree[tree_key[index]].append(i.eid)
            index += 1
        return [self.g.word.get(j).name for j in tree_key][:self.max_count]

    def bfs_export(self):
        word_obj = self.g.word.index.lookup(name=self.start_word).next()
        tree_key = []
        search_stack = [word_obj.eid]
        while len(search_stack) != 0 and len(tree_key) < self.max_count:
            current_node = search_stack.pop()
            tree_key.append(current_node)
            near_words = self.g.word.get(current_node).bothV()
            if near_words == None:
                continue
            near_words = sorted(list(near_words), key=lambda k: k.weight)
            for i in near_words:
                if i.eid not in tree_key and i.eid not in search_stack:
                    search_stack.append(i.eid)
        return [self.g.word.get(j).name for j in tree_key][:self.max_count]

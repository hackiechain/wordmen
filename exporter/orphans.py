from exporter import Exporter
from random import sample

class Orphans(Exporter):
    def init(self, max_count=90):
        self.max_count = int(max_count) 
    
    def export(self):
        orphans = []
        for i in self.g.word.get_all():
            if i.bothE() == None:
                orphans.append(i)
        orphans = sorted(list(orphans), key=lambda k: k.weight)
        words = []
        for i in orphans[:self.max_count * 2]:
            words.append(i.name)
        if self.max_count > len(words) :
            return words 
        return sample(set(words), self.max_count)        

#!/usr/bin/python
from __future__ import print_function
import time, argparse, sys
from scikits.audiolab import Sndfile, play
from progressbar import ProgressBar, Percentage, Bar, ETA, SimpleProgress
from bulbs.neo4jserver import Graph
from model import Word, Similar, SameStem, Synonym
import analyzer, exporter
from random import shuffle, sample
import downloader
import ujson as json
import os

def reflection(module_name, class_name, param):
    global g
    kwds = {}
    if param != None:
        for i in param:
            k, s, v = i.partition("=")
            kwds[k] = v 
    module = __import__(module_name, fromlist="%s" % (class_name))
    instance = getattr(module, class_name)(g)
    instance.init(**kwds)
    return instance

def do_load(fd):
    global g
    content = fd.read().split("\n")
    proxy_instance = getattr(g, 'word')
    pbar = ProgressBar(widgets=[Percentage(), " ", ETA(), Bar(), SimpleProgress()], maxval=len(content)).start()
    i = 0 
    for line in content:
        if len(line.rstrip()) != 0:
            proxy_instance.get_or_create("name", line.rstrip(), name=line.rstrip())
        pbar.update(i)
        i += 1
    pbar.finish()

def do_analyze(class_name, param):
    obj = reflection("analyzer", class_name, param)
    element_list = obj.list()
    list_len = len(element_list)
    pbar = ProgressBar(widgets=[Percentage(), " ", ETA(), Bar(), SimpleProgress()], maxval=sum(range(list_len))).start()
    count = 0 
    for i in range(0, list_len):
        for j in range(i + 1, list_len):
            obj.analyze(element_list[i], element_list[j])
            count += 1
            pbar.update(count)
    pbar.finish()
    
def do_relate_op(word1, word2, is_delete):
    delete_relation = "start n=node(%s),m=node(%s) match n-[r]-m delete r"    
    create_relation = "START left=node(%s), right=node(%s)  CREATE UNIQUE left-[r:Similar]->right RETURN r"
    global g
    word1_obj = g.word.index.lookup(name=word1).next()
    word2_obj = g.word.index.lookup(name=word2).next()
    if is_delete:
        g.cypher.execute(delete_relation % (word1_obj.eid, word2_obj.eid))
    else:
        g.cypher.execute(create_relation % (word1_obj.eid, word2_obj.eid))
    
def do_list(fd, class_name, param):
    obj = reflection("exporter", class_name, param)
    rt = obj.export()
    for i in rt:
        print(i, file=fd)
    fd.close()
    
def play_sound(file_path):
    wave_file = Sndfile(file_path, 'r')
    signal = wave_file.read_frames(wave_file.nframes)
    old_fd = sys.stdout
    try:
        wave_file = Sndfile(file_path, 'r')
        signal = wave_file.read_frames(wave_file.nframes)
        old_fd = sys.stdout
        while  True:
            try:
                sys.stdout = open("/dev/null", 'w')
                play(signal, wave_file.samplerate)
                break
            except Exception, e:
                print(e)
                continue
        sys.stdout = old_fd
    except Exception, e:
        pass

def sound_word(word_obj):
    for i in range(3):
        path = "word_sound/%s_%s.mp3" % (word_obj.name, i)
        if os.path.isfile(path):
            play_sound(path)

def print_word(word_obj):
    if len(word_obj.definitions_zh)!=0:
        word_def = "\n".join(json.loads(word_obj.definitions_zh))
        print(word_def)    

def do_submit(fd, is_sound):
    global g
    content = fd.read().split("\n")
    shuffle(content)
    for line in content:
        if len(line.rstrip()) != 0:    
            obj = g.word.index.lookup(name=line.rstrip())
            if obj == None:
                continue
            else:
                word_obj = obj.next()
                word_test(word_obj,is_sound)

def play_word_sound(word_name, count):
    for i in range(count):
        print("word_sound/%s_%s.wav" % (word_name, i))
        play_sound("word_sound/%s_%s.wav" % (word_name, i))
        
def word_test(word_obj, is_sound):
    weight_offset = 0
    while True:
        if is_sound:
            play_word_sound(word_obj.name, word_obj.sound_count)
        rt = raw_input("%s remember?(y/n)" % (word_obj.name))
        if rt == "y":
            weight_offset = 1
            break
        elif rt == "n":
            weight_offset = 0
            break
        elif rt == "p":
            play_word_sound(word_obj.name, word_obj.sound_count)
            print(word_obj.name)
            print('\n'.join(json.loads(word_obj.definitions_zh)))
        else:
            continue
    print_word(word_obj)
    word_obj.weight += weight_offset
    word_obj.save()

def do_sample(count, weight, sound):
    global g
    word_list = list(g.word.get_all())
    shuffle(word_list)
    i = 0 
    for word_obj in word_list:
        if count == i:
                break        
        if weight == word_obj.weight:
            word_test(word_obj, sound)
            i += 1
    
def do_report():
    global g
    query = "START n=node:word(weight = '%s') RETURN count(n)"
    query2 = "START n=node:word('name: *') WHERE n.weight > %s RETURN count(n)"
    query3 = "START n=node:word('name: *') RETURN count(n)"
    total_word = g.cypher.execute(query3).one().raw
    for i in range(0, 5):
        word_num = g.cypher.execute(query % (i)).one().raw
        print("weight %s: %s %.4f%%" % (i, word_num, word_num / float(total_word) * 100))
    word_num = g.cypher.execute(query2 % (6)).one().raw
    print("weight >%s: %s %.4f%%" % (6, word_num, word_num / float(total_word) * 100))

def do_fetch():
    global g
    word_list = list(g.word.get_all())
    prepared_list = []
    for word_obj in word_list:
        if word_obj.definitions_en == "" :
            prepared_list.append(word_obj.name)
    bar_index = 0
    
    print("Fetching word definition")
    pbar = ProgressBar(widgets=[Percentage(), " ", ETA(), Bar(), SimpleProgress()], maxval=len(prepared_list)).start()
    for w, i in downloader.fetch_def(prepared_list):
        obj = g.word.index.lookup(name=w).next()
        rt = json.dumps(i)
        obj.definitions_zh = rt 
        pbar.update(bar_index)
        bar_index += 1
        obj.save()
    pbar.finish()
    return
    print("Fetching word popularity")
    bar_index = 0
    pbar = ProgressBar(widgets=[Percentage(), " ", ETA(), Bar(), SimpleProgress()], maxval=len(prepared_list)).start()
    for w, popularity in downloader.fetch_popularity(prepared_list):
        obj = g.word.index.lookup(name=w).next()
        obj.popularity = popularity
        obj.save()
        pbar.update(bar_index)
        bar_index += 1
    pbar.finish()    
    
    print("Fetching word sound")
    bar_index = 0
    pbar = ProgressBar(widgets=[Percentage(), " ", ETA(), Bar(), SimpleProgress()], maxval=len(prepared_list)).start()
    for w, sound_count in downloader.fetch_sound(prepared_list, "word_sound"):
        obj = g.word.index.lookup(name=w).next()
        obj.sound_count = sound_count
        obj.save()
        pbar.update(bar_index)
        bar_index += 1
    pbar.finish()
    
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Word Memorizing Engine")
    
    subparsers = parser.add_subparsers(dest="cmd_name")
    # Sub-command load
    cmd_load = subparsers.add_parser('load', help='Load words list and create nodes')
    cmd_load_group = cmd_load.add_argument_group()
    cmd_load_group.add_argument("-f", dest="file", type=argparse.FileType('r'), required=True,
                        help="List file path.")
    
    # Sub-command load
    cmd_fetch = subparsers.add_parser('fetch', help='fetch words sound and definition')
    
    # Sub-command analyze
    cmd_analyze = subparsers.add_parser('analyze', help='Analyze and build relevance between nodes')
    cmd_analyze_group = cmd_analyze.add_argument_group()
    cmd_analyze_group.add_argument("-c", dest="class_name", action="store", required=True, choices=analyzer.__all__,
                        help="Class name of the relevance analyzer.")
    cmd_analyze_group.add_argument("-p", dest="param", nargs='*', action="store",
                        help="Arguments that will pass to the analyzer instance.")    
    
    # Sub-command relate
    cmd_relate = subparsers.add_parser('relate', help='Add or delete relation')
    cmd_relate_group = cmd_relate.add_argument_group()
    cmd_relate_group.add_argument(dest="words", action="store", nargs=2,
                        help="words to be related.")
    cmd_relate_group.add_argument("-d", dest="delete", action="store_true",
                        help="is delete?")    
    
    # Sub-command list
    cmd_list = subparsers.add_parser('list', help='Generate word list')
    cmd_list_group = cmd_list.add_argument_group()
    cmd_list_group.add_argument("-c", dest="class_name", action="store", default="mixed", choices=exporter.__all__,
                        help="Class name of the export helper.")
    cmd_list_group.add_argument("-p", dest="param", nargs='*', action="store",
                        help="Arguments that will pass to the export instance.")  
    cmd_list_group.add_argument("-f", dest="file", type=argparse.FileType('w+'), required=True,
                        help="List file target.")
    
    # Sub-command submit
    cmd_commit = subparsers.add_parser('submit', help='Submit your learning progress')
    cmd_commit_group = cmd_commit.add_argument_group()
    cmd_commit_group.add_argument("-f", dest="file", type=argparse.FileType('r'), required=True,
                        help="word list")    
    cmd_commit_group.add_argument("-s", dest="sound", action="store_true",
                        help="play word sound")
    
    # Sub-command sample
    cmd_sample = subparsers.add_parser('sample', help='Random choose some word')
    cmd_sample_group = cmd_sample.add_argument_group()
    cmd_sample_group.add_argument("-c", dest="count", type=int, required=True,
                        help="word count")       
    cmd_sample_group.add_argument("-w", dest="weight", type=int, required=True,
                        help="word weight")
    cmd_sample_group.add_argument("-s", dest="sound", action="store_true",
                        help="play word sound")
   
    # Sub-command report
    cmd_report = subparsers.add_parser('report', help='Report your learning status')
    cmd_report_group = cmd_report.add_argument_group()
    
    args = parser.parse_args()
    
    global g
    g = Graph()
    g.add_proxy("word", Word)
    g.add_proxy("similar", Similar)
    g.add_proxy("stem", SameStem)
    g.add_proxy("synonym", Synonym)
    
    if args.cmd_name == "load":
        do_load(args.file)
    elif args.cmd_name == "analyze":
        do_analyze(args.class_name, args.param)
    elif args.cmd_name == "list":
        do_list(args.file, args.class_name, args.param)    
    elif args.cmd_name == "relate":
        do_relate_op(args.words[0], args.words[1], args.delete)
    elif args.cmd_name == "submit":
        do_submit(args.file, args.sound)
    elif args.cmd_name == "sample":
        do_sample(args.count, args.weight, args.sound)
    elif args.cmd_name == "report":
        do_report()
    elif args.cmd_name == "fetch":
        do_fetch()
        

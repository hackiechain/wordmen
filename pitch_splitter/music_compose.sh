#!/bin/bash
file="$1"
out_put="$2"
if [ "$2" != "" ]; then
    wav_dir="./word_wav"
    sox $(while IFS= read -r line;do echo "$wav_dir/$line.wav" silence2.wav; done <"$file") $out_put
else
    echo "no output filename"
fi 
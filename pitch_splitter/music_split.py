from __future__ import print_function
import subprocess
import sys, os, time
from scikits.audiolab import Sndfile, play
import scipy, pylab, numpy
from progressbar import ProgressBar, Percentage, Bar, ETA, SimpleProgress
from scipy.ndimage.filters import gaussian_filter

def search_silence(signal, min_duration, sample_rate):
    silence_list = []
    silence_start = 0
    silence_end = -1
    pbar = ProgressBar(widgets=[Percentage(), " ", ETA(), Bar(), SimpleProgress()], maxval=len(signal)).start() 
    for i, amplitude in enumerate(signal):    
        pbar.update(i)
        if amplitude == 0:
            if silence_start != 0:
                continue
            else:
                silence_start = i
        else:
            if silence_start != 0:
                silence_end = i
                if (silence_end - silence_start) >= min_duration:    
                    silence_list.append((silence_start, silence_end))
                silence_start = 0
                silence_end = -1
        
    pbar.finish()
    return silence_list

def silence_detect(file_name, split_log, min_duration_sec):
    wave_file = Sndfile(file_name, 'r')
    signal = wave_file.read_frames(wave_file.nframes)
    channels = wave_file.channels
    sample_rate = wave_file.samplerate
    silence_list = search_silence(signal[:, 0], min_duration_sec * sample_rate, sample_rate)
    split_list = silence_to_split(silence_list, len(signal))
    fd = open(split_log, "w+")
    for start, end in split_list:
        fd.write("%s %s\n" % (start, end))
    fd.close()

def run_split(split_list, sound_file, word_list, dir_name, sample_rate):
    try:
        os.mkdir(dir_name)
    except OSError, e:
        pass
    for i, a_split in enumerate(split_list):
        start, end = a_split
        cmd = "sox %s %s/%s.wav trim %s %s" % (sound_file, dir_name, word_list[i], start / float(sample_rate),
                                                (end - start) / float(sample_rate))
        subprocess.check_output(cmd, shell=True)

def pitch_split(sound_file, split_list, go_to, debug_level):
    wave_file = Sndfile(sound_file, 'r')
    signal = wave_file.read_frames(wave_file.nframes)
    channels = wave_file.channels
    sample_rate = wave_file.samplerate
    new_split_list = []
    index = -1
    if debug_level == 0:
        pbar = ProgressBar(widgets=[Percentage(), " ", ETA(), Bar(), SimpleProgress()], maxval=len(split_list)).start()
    previous_end = 0
    for start, end in split_list:
        index += 1
        if index < go_to:
            continue
        # smoothed_signal = gaussian_filter(signal[start:end, 0], 10)
        if debug_level > 1:
            play_sound(signal[start:end, 0])
        result = gender_detect(signal[start:end, 0], sample_rate, debug_level)
        if debug_level > 0:
            print("%s %s" % (index, result))
        if result.startswith("W"):
            new_split_list.append((previous_end, start))
            previous_end = start
        if debug_level == 0:
            pbar.update(index)
    new_split_list.append((previous_end, len(signal)))
    if debug_level == 0:
        pbar.finish()
    return new_split_list

def play_sound(signal):
    old_fd = sys.stdout
    while  True:
        try:
            sys.stdout = open("/dev/null", 'w')
            play(signal)
            break
        except Exception, e:
            continue
    sys.stdout = old_fd

def human_aided(gender, signal, f):
    rt = None
    while rt == None:
        play_sound(signal)
        rt = raw_input("Sounds like %s, gender?" % (gender))
        if rt == "r":
            rt = None
        elif rt == "p":
            X = numpy.fft.fft(signal)
            Xdb = 20 * scipy.log10(scipy.absolute(X))
            pylab.plot(f, Xdb)
            pylab.xlim(75, 275)
            pylab.ylim(0, 60)
            pylab.show()
            rt = None
        elif rt == "w":
            gender = "W"
            break
        elif rt == "m":
            gender = "M"
            break            
        else:
            rt = None
    return gender

def gender_detect(signal, sample_rate, debug_level):
    X = numpy.fft.rfft(signal)
    Xdb = 20 * scipy.log10(scipy.absolute(X))
    f = scipy.linspace(0, sample_rate, len(signal), endpoint=False)
    mam_start = None
    man_end = None
    woman_start = None
    woman_end = None
    human_start = None
    human_end = None
    for i, v in enumerate(f):
        if v > 65 and human_start == None:
            human_start = i
        if v > 85 and mam_start == None :
            mam_start = i
        if v > 180 and man_end == None :
            man_end = i
        if v > 165 and woman_start == None :
            woman_start = i                        
        if v > 255 and woman_end == None:
            woman_end = i
        if v > 275 and human_end == None:
            human_end = i
    man_freq_db = abs(numpy.average(Xdb[mam_start:man_end]))
    mam_specific_max = abs(numpy.max(Xdb[mam_start:woman_start]))
    woman_freq_db = abs(numpy.average(Xdb[woman_start:woman_end]))
    womam_specific_max = abs(numpy.max(Xdb[man_end:woman_end]))
    human_low_max = abs(numpy.max(Xdb[human_start:mam_start]))
    human_high_max = abs(numpy.max(Xdb[woman_end:human_end]))
    freq_var = numpy.var(numpy.array([man_freq_db, woman_freq_db]))
    gender = None
    data = "(%.4f %.4f %.4f %.4f %.4f %.4f) %.4f" % (man_freq_db, mam_specific_max,
                                         woman_freq_db, womam_specific_max,
                                         human_low_max, human_high_max, float(freq_var))
    
    if mam_specific_max < 20 and womam_specific_max < 20 and human_high_max < 20:
        gender = "M ?"
    else:
        if freq_var > 40:
            gender = "W"
        elif freq_var <= 40 and freq_var > 10:
            if human_high_max > human_low_max:
                if numpy.var(numpy.array([human_high_max, human_low_max])) > 5:
                    gender = "W"
                elif womam_specific_max > mam_specific_max \
                    and numpy.var(numpy.array([womam_specific_max, mam_specific_max])) > 10:
                    gender = "W"
                else:
                    gender = "M"
            elif human_high_max < human_low_max:
                if womam_specific_max > mam_specific_max \
                    and numpy.var(numpy.array([womam_specific_max, mam_specific_max])) > 15:
                    gender = "W"
                else:
                    gender = "M"
            else:
                gender = "M"

            print(data)
            gender = human_aided(gender, signal, f)                  

        else:
            gender = "M"
    return  "%s %s" % (gender, data)

def silence_to_split(silence_list, max_end):
    previous_end = 0
    split_list = []
    for start, end in silence_list:
        split_list.append((int(previous_end), int(start)))
        previous_end = end
    split_list.append((previous_end, max_end))
    return split_list

def load_split_log(split_log):
    content = open(split_log, 'r').read().split("\n")
    split_list = []
    for i in content:
        if i.rstrip() == "":
            continue
        start, splitter, end = i.partition(" ")
        split_list.append((int(start), int(end)))
    return split_list

if __name__ == "__main__":
    cmd = sys.argv[1]
    if cmd == "split":
        sound_file = sys.argv[2]
        split_log = sys.argv[3]
        word_log = sys.argv[4]
        dir_name = sys.argv[5]
        sample_rate = 44100
        split_list = load_split_log(split_log)
        word_list = open(word_log, 'r').read().split("\n")
        run_split(split_list, sound_file, word_list, dir_name, sample_rate)
        
    elif cmd == "pitch":
        sound_file = sys.argv[2]
        old_split_log = sys.argv[3]
        new_split_log = sys.argv[4]
        go_to = sys.argv[5]
        debug_level = sys.argv[6]
        split_list = load_split_log(old_split_log)
        new_split_list = pitch_split(sound_file, split_list, int(go_to), int(debug_level))
        fd = open(new_split_log, "w+")
        for start, end in new_split_list:
            fd.write("%s %s\n" % (start, end))
        fd.close()
        
    elif cmd == "silence":
        sound_file = sys.argv[2]
        split_log = sys.argv[3]
        min_duration_sec = sys.argv[4]        
        silence_detect(sound_file, split_log, float(min_duration_sec))
        
        

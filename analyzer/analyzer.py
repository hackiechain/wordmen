class Analyzer(object):
    def __init__(self, db):
        self.g = db
    
    def init(self, *argv, **kwds):
        pass
    
    def list(self):
        return list(self.g.word.get_all())
    
    def analyze(self, word1, word2):
        pass
        
    def has_relation(self, obj1, obj2, relation_label):
        a = self.g.cypher.execute("start n1=node(%s),n2=node(%s) where n1-[:%s]->n2 return count(*)"\
                             % (obj1.eid, obj2.eid, relation_label)).one()
        return int(a.raw) != 0

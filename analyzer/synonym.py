from analyzer import Analyzer
from nltk.corpus import wordnet

class Synonym(Analyzer):
    label = "Synonym"
    
    def init(self):
        pass
    
    def analyze(self, word1_obj, word2_obj):
        syn_sets = wordnet.synsets(word2_obj.name)
        if word1_obj.name in self.syn_list(syn_sets):
            self.relate(word1_obj, word2_obj)
        syn_sets = wordnet.synsets(word1_obj.name)
        if word2_obj.name in self.syn_list(syn_sets):
            self.relate(word2_obj, word1_obj)  

    def syn_list(self, syn_sets):
        l = []
        for syn_set in syn_sets:
            for syn in syn_set.lemma_names:
                l.append(syn)
        return l
    
    def relate(self, word1_obj, word2_obj):
        if not self.has_relation(word1_obj, word2_obj, self.label):
            self.g.synonym.create(word1_obj, word2_obj)

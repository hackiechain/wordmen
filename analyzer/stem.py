from analyzer import Analyzer
from nltk.stem.lancaster import LancasterStemmer

class Stem(Analyzer):
    label = "SameStem"
    
    def init(self):
        self.stemmer = LancasterStemmer()
        
    def analyze(self, word1_obj, word2_obj):
        stem_1 = self.stemmer.stem(word1_obj.name)
        stem_2 = self.stemmer.stem(word2_obj.name)
        if stem_1 == stem_2:
            self.relate(word1_obj, word2_obj)
            self.relate(word2_obj, word1_obj)

    def relate(self, word1_obj, word2_obj):
        if not self.has_relation(word1_obj, word2_obj, self.label):
            self.g.stem.create(word1_obj, word2_obj)

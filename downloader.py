# encoding: utf-8
from lxml import etree, html
import urllib2, urllib
from multiprocessing.pool import ThreadPool
import re
import os
sound_path = None

def read_url(url):
    count = 2 
    data = ""
    while count > 0:
        try:
            response = urllib2.urlopen(url)
            data = response.read()
            break     
        except Exception, e:
            count -= 1 
    return data           

def download_url(url, file_path):
    count = 2 
    while count > 0:
        try:
            urllib.urlretrieve(url, file_path)
            break
        except Exception, e:
            count -= 1    

def get_definition(word):
    xml = read_url('http://dict.youdao.com/fsearch?q=%s' % (word))
    xml_obj = etree.fromstring(xml)
    definition_list = []
    for i in xml_obj.findall(".//custom-translation/translation"):
        for j in i.itertext():
            definition_list.append(j)
    return word, definition_list

def fetch_sound_id(word):
    try:
        xml = read_url('http://www.merriam-webster.com/dictionary/%s' % (word))
        html_obj = html.fromstring(xml)
        sound_id_list = []
        for i in html_obj.xpath('.//input[@class="au"]'):
            sound_str = re.findall(r'au\((.+)\);', i.get('onclick'))[0]
            sound_id , token, sound_word = sound_str.partition(",")
            if sound_word.strip('\' ') == word:
                sound_id_list.append(sound_id.strip('\''))
        return sound_id_list
    except Exception, e:
        return []

def download_sound(word):
    global sound_path
    sound_id = fetch_sound_id(word)
    if len(sound_id) == 0:
        return word, 0
    prefix = sound_id[0][0]
    for i, v in enumerate(sound_id):
        download_url("http://media.merriam-webster.com/soundc11/%s/%s.wav" % (prefix, v),
                     "%s/%s_%s.wav" % (sound_path, word, i))
    return word, len(sound_id)

def get_popularity(word):
    xml = read_url('http://www.collinsdictionary.com/dictionary/english/%s' % (word))
    html_obj = html.fromstring(xml)
    i = html_obj.xpath('.//img[@class="commonness_image"]')
    popularity = int(i[0].get('data-band'))
    return word, popularity

def fetch_popularity(word_list):
    pool = ThreadPool(processes=10)
    result = pool.imap(get_popularity, word_list, chunksize=2)
    return result

def fetch_def(word_list):
    pool = ThreadPool(processes=10)
    result = pool.imap(get_definition, word_list, chunksize=2)
    return result
        
def fetch_sound(word_list, path):
    global sound_path
    sound_path = path
    pool = ThreadPool(processes=15)
    result = pool.imap(download_sound, word_list, chunksize=2)
    return result
